// ==UserScript==
// @name         Nation's Roll20 shizznit
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Cool shit happens to the tracker. Requires R20es https://ssstormy.github.io/roll20-enhancement-suite/
// @author       nation
// @match        https://app.roll20.net/editor/
// @grant        none
// ==/UserScript==

const trackerObjects = {};

(function() {
    'use strict';
    const isGm = window.is_gm;
    const initiativeTrackerId = 'initiativewindow';
    const initiativeTrackerContainerSelector = '#initiativewindow:not(.observing)';
    const initiativeTrackerTokensSelector = '#initiativewindow ul:not(.header) li.token';
    const childrenClassNameValue = 'initiative editable ui-droppable';
    const dedupeClassName = 'observing';
    const observerParams = {childList: true, attributes: true, subtree: true, characterData: true};

    const observer = new MutationObserver((mutationsList) => {
        let idx = 0;
        let target;
        while (!target && mutationsList.length >= idx) {
            target = mutationsList[idx].target.closest('#initiativewindow');
            idx++;
        }
        console.log(mutationsList);
        console.log(target);
        observer.disconnect();
        doTheThing(target);
        observer.observe(target, observerParams);
        console.log('finished');
    });
    const bodyObserver = new MutationObserver(setup);

    function setup() {
        const trackerContainer = document.querySelector(initiativeTrackerContainerSelector);
        if (trackerContainer) {
            trackerContainer.classList.add(dedupeClassName);
            observer.disconnect();
            observer.observe(trackerContainer, observerParams);
            doTheThing(trackerContainer);
        }
    }

    function getTokenModel(token) {
        if (token._model) {
            return token._model;
        }
        if (token.model) {
            return token.model;
        }
        return null;
    }

    const watchedProps = {
        bar1_value: '#hp',
        bar2_value: '#ac',
        bar3_value: '#ms'
    }

    const handler = {
        set: function(obj, prop, value) {
            if (Object.keys(watchedProps).indexOf(prop) >= 0 && trackerObjects[obj.id]) {
                const trackerElement = trackerObjects[obj.id];
                const elem = trackerElement.querySelector(watchedProps[prop]);
                elem.innerText = value;
            }
            obj[prop] = value;
            return true;
        }
    };

    function doTheThing(trackerContainer) {
        console.log('running');
        const trackerTokenElements = document.querySelectorAll(initiativeTrackerTokensSelector);
        const players = window.Campaign.characters.models;
        const currentTokens = window.d20.engine.canvas.getObjects();
        let first = true;
        let header;
        const appendHeader = trackerContainer && trackerContainer.prepend && trackerContainer.children.length === 1;
        const headerContainer = document.createElement('ul');
        headerContainer.classList.add('header');
        for (const trackerElement of trackerTokenElements) {
            if (first && appendHeader) {
                header = trackerElement.cloneNode(true);
                while (header.children.length > 2) {
                    header.removeChild(header.children[1]);
                }

                header.setAttribute('data-tokenid', '');

                const headerHpClone = header.children[0].cloneNode(true);
                const headerAcClone = header.children[0].cloneNode(true);
                const headerMsClone = header.children[0].cloneNode(true);

                header.children[0].innerText = 'Init';
                headerHpClone.innerText = 'HP';
                headerAcClone.innerText = 'AC';
                headerMsClone.innerText = 'MS';

                header.children[0].style.lineHeight = '1em';
                headerHpClone.style.lineHeight = '1em';
                headerAcClone.style.lineHeight = '1em';
                headerMsClone.style.lineHeight = '1em';

                header.children[0].style.textAlign = 'center';
                headerHpClone.style.textAlign = 'center';
                headerAcClone.style.textAlign = 'center';
                headerMsClone.style.textAlign = 'center';

                header.children[0].style.width = '50px';
                headerHpClone.style.width = '50px';
                headerAcClone.style.width = '50px';
                headerMsClone.style.width = '50px';

                // header.prepend(headerMsClone);
                header.prepend(headerAcClone);
                header.prepend(headerHpClone);

                header.style.background = 'none';

                headerContainer.prepend(header);

                first = false;
            }
            const identifier = trackerElement.getAttribute('data-tokenid');
            const token = currentTokens.find(({model}) => model.get('id') === identifier);
            const tokenModel = getTokenModel(token);
            const charId = tokenModel.view.model.character.id;
            const char = players.find((player) => player.id === charId);
            const isPlayer = !!char.attributes.controlledby;
            const acAttribName = isPlayer ? 'ac' : 'npc_ac';
            const models = tokenModel.character.attribs.models;
            const name = token._model.character.attributes.name;

            const nameElem = trackerElement.querySelector('span.name');
            if (nameElem && nameElem.innerText !== name) {
                nameElem.innerText = name;
            }

            if (isGm || isPlayer) {
                trackerObjects[identifier] = trackerElement;
            }

            const attributesProxy = new Proxy(tokenModel.attributes, handler);
            tokenModel.originalAttributes = tokenModel.attributes;
            tokenModel.attributes = attributesProxy;

            const hpElemFound = !!trackerElement.querySelector('#hp');
            const acElemFound = !!trackerElement.querySelector('#ac');
            const msElemFound = !!trackerElement.querySelector('#ms');

            const hpClone = hpElemFound ? trackerElement.querySelector('#hp') : trackerElement.children[0].cloneNode(true);
            const acClone = acElemFound ? trackerElement.querySelector('#ac') : trackerElement.children[0].cloneNode(true);
            const msClone = msElemFound ? trackerElement.querySelector('#ms') : trackerElement.children[0].cloneNode(true);

            hpClone.innerText = !isPlayer && !isGm ? '?' : tokenModel.attributes.bar1_value;
            acClone.innerText = !isPlayer && !isGm ? '?' : tokenModel.attributes.bar2_value;
            msClone.innerText = !isPlayer && !isGm ? '?' : tokenModel.attributes.bar3_value;

            hpClone.id = 'hp';
            acClone.id = 'ac';
            msClone.id = 'ms';

            hpClone.style.textAlign = 'center';
            acClone.style.textAlign = 'center';
            trackerElement.children[0].style.textAlign = 'center';

            hpClone.style.lineHeight = '1em';
            acClone.style.lineHeight = '1em';
            trackerElement.children[0].style.lineHeight = '1em';

            hpClone.style.width = '50px';
            acClone.style.width = '50px';
            trackerElement.children[0].style.width = '50px';

            // if (!msElemFound) {
            //     trackerElement.prepend(msClone);
            // }
            if (!acElemFound) {
                trackerElement.prepend(acClone);
            }
            if (!hpElemFound) {
                trackerElement.prepend(hpClone);
            }
        }
        if (appendHeader) {
            trackerContainer.prepend(headerContainer);
        }
    }

    function watchContainer() {
        console.log('=========================================================================================');
        const container = document.querySelector('.ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-draggable.ui-resizable');
        if (container) {
            console.log('container found');
            bodyObserver.observe(container, {childList: true, subtree: true});
            container.classList.add('testtemp');
            setup();
            return true;
        } else {
            console.log('container not found');
            return false;
        }
    }
    function onLoad() {
        const configContainerResult = watchContainer();
        if (!configContainerResult) {
            const tracker = document.getElementById('startrounds');
            if (tracker) {
                tracker.onclick = () => setTimeout(watchContainer, 500);
            } else {
                setTimeout(watchContainer, 10000);
            }
        }
    }
    window.addEventListener('load', () => setTimeout(onLoad, 10000), false);
})();
